package id.sudruntech.statusdownloaderforwhatsapp;

import android.content.Context;
import android.content.Intent;

import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;

import butterknife.BindView;
import id.sudruntech.base.BaseActivity;
import id.sudruntech.base.widget.CustomTextView;
import id.sudruntech.statusdownloaderforwhatsapp.adapter.MainViewPager;
import id.sudruntech.statusdownloaderforwhatsapp.fragment.DownloadFragment;
import id.sudruntech.statusdownloaderforwhatsapp.fragment.ImageFragment;
import id.sudruntech.statusdownloaderforwhatsapp.fragment.VideoFragment;

public class MainActivity extends BaseActivity {

    @BindView(R.id.root_view)
    ConstraintLayout rootView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tv_title)
    CustomTextView tvTitle;
    @BindView(R.id.tab)
    TabLayout tab;
    @BindView(R.id.view_pager)
    ViewPager viewPager;

    public static void start(Context context) {
        context.startActivity(new Intent(context, MainActivity.class));
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void initViews() {
        setActionBar(toolbar, tvTitle, getString(R.string.app_name_toolbar), false, false);
        storagePermission(granted -> {
            if (granted) {
                setupViewPager();
            } else {
                Snackbar.make(rootView, getString(R.string.msg_no_write_external_permission), Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    private void setupViewPager() {
        MainViewPager mainViewPager = new MainViewPager(getSupportFragmentManager(), FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        mainViewPager.addFragment(ImageFragment.newInstance(), getString(R.string.images));
        mainViewPager.addFragment(VideoFragment.newInstance(), getString(R.string.videos));
        mainViewPager.addFragment(DownloadFragment.newInstance(), getString(R.string.downloads));
        viewPager.setAdapter(mainViewPager);
        viewPager.setOffscreenPageLimit(mainViewPager.getCount());
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tab));
        tab.setupWithViewPager(viewPager);

        for (int i = 0; i < tab.getTabCount(); i++) {
            TabLayout.Tab customTab = tab.getTabAt(i);
            if (customTab != null) {
                customTab.setCustomView(mainViewPager.getTabView(tab, i));
            }
        }
    }
}