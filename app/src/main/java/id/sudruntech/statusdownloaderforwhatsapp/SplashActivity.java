package id.sudruntech.statusdownloaderforwhatsapp;

import id.sudruntech.base.BaseActivity;

public class SplashActivity extends BaseActivity {

    @Override
    protected boolean getWindowFullScreen() {
        return true;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_splash;
    }

    @Override
    protected void initViews() {
        Thread timerSplash = new Thread() {
            @Override
            public void run() {
                try {
                    short timerSplash = 0;
                    while (timerSplash < 1700) {
                        sleep(100);
                        timerSplash = (short) (timerSplash + 100);
                    }

                    MainActivity.start(SplashActivity.this);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    finish();
                }
            }
        };

        timerSplash.start();
    }
}