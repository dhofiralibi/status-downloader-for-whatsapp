package id.sudruntech.statusdownloaderforwhatsapp.utils;

import android.os.Environment;

import java.io.File;

public class ConstantUtils {

    public static final File STATUS_DIRECTORY = new File(String.format("%s%sWhatsApp/Media/.Statuses", Environment.getExternalStorageDirectory(), File.separator));
    public static final String DOWNLOADS_DIRECTORY = String.format("%s%sStatusDownloader", Environment.getExternalStorageDirectory(), File.separator);
    public static final String APP_DIRECTORY = String.format("%s%sStatusDownloader", Environment.getExternalStorageDirectory(), File.separator);
    public static final int THUMB_SIZE = 128;
}
