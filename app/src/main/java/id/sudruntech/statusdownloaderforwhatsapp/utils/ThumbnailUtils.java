package id.sudruntech.statusdownloaderforwhatsapp.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.provider.MediaStore;

import java.io.File;

public class ThumbnailUtils {

    public static Bitmap imageThumbnail(File file) {
        return android.media.ThumbnailUtils.extractThumbnail(BitmapFactory.decodeFile(file.getAbsolutePath()), ConstantUtils.THUMB_SIZE, ConstantUtils.THUMB_SIZE);
    }

    public static Bitmap videoThumbnail(File file) {
        return android.media.ThumbnailUtils.createVideoThumbnail(file.getAbsolutePath(), MediaStore.Video.Thumbnails.MICRO_KIND);
    }
}
