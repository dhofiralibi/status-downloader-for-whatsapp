package id.sudruntech.statusdownloaderforwhatsapp.model;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

import java.io.File;

public class Status implements Parcelable {

    private static final String MP4 = ".mp4";
    private File file;
    private Bitmap thumbnail;
    private String title;
    private String path;
    private boolean isVideo;

    public Status(File file, String title, String path) {
        this.file = file;
        this.title = title;
        this.path = path;
        this.isVideo = file.getName().endsWith(MP4);
    }

    public void setThumbnail(Bitmap thumbnail) {
        this.thumbnail = thumbnail;
    }

    public File getFile() {
        return file;
    }

    public Bitmap getThumbnail() {
        return thumbnail;
    }

    public String getTitle() {
        return title;
    }

    public String getPath() {
        return path;
    }

    public boolean isVideo() {
        return isVideo;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeSerializable(this.file);
        dest.writeParcelable(this.thumbnail, flags);
        dest.writeString(this.title);
        dest.writeString(this.path);
        dest.writeByte(this.isVideo ? (byte) 1 : (byte) 0);
    }

    private Status(Parcel in) {
        this.file = (File) in.readSerializable();
        this.thumbnail = in.readParcelable(Bitmap.class.getClassLoader());
        this.title = in.readString();
        this.path = in.readString();
        this.isVideo = in.readByte() != 0;
    }

    public static final Parcelable.Creator<Status> CREATOR = new Parcelable.Creator<Status>() {
        @Override
        public Status createFromParcel(Parcel source) {
            return new Status(source);
        }

        @Override
        public Status[] newArray(int size) {
            return new Status[size];
        }
    };
}
