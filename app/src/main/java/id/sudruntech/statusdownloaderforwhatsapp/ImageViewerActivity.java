package id.sudruntech.statusdownloaderforwhatsapp;

import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.util.Pair;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.google.android.material.snackbar.Snackbar;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

import butterknife.BindView;
import butterknife.OnClick;
import id.sudruntech.base.BaseActivity;
import id.sudruntech.statusdownloaderforwhatsapp.model.Status;
import id.sudruntech.statusdownloaderforwhatsapp.utils.ConstantUtils;

public class ImageViewerActivity extends BaseActivity {

    @BindView(R.id.root_view)
    CoordinatorLayout rootView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.imv_photo)
    ImageView imvPhoto;
    @BindView(R.id.btn_download)
    ImageButton btnDownload;

    private static final String EXTRA_STATUS = "extra_status";
    private static final String EXTRA_DOWNLOAD = "extra_download";

    private Status status;

    public static void start(Context context, ImageView imageView, boolean isDownload, Status status) {
        Intent intent = new Intent(context, ImageViewerActivity.class);
        intent.putExtra(EXTRA_STATUS, status);
        intent.putExtra(EXTRA_DOWNLOAD, isDownload);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            ActivityOptions activityOptions = ActivityOptions.makeSceneTransitionAnimation((Activity) context,
                    Pair.create(imageView, context.getResources().getString(R.string.image_transition)));
            context.startActivity(intent, activityOptions.toBundle());
        } else {
            context.startActivity(intent);
        }
    }

//    @Override
//    protected boolean getWindowFullScreen() {
//        return true;
//    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_image_viewer;
    }

    @Override
    protected void initViews() {
        setActionBar(toolbar, null, "", false, true);

        initWindows();

        status = getIntent().getParcelableExtra(EXTRA_STATUS);
        boolean isDownload = getIntent().getBooleanExtra(EXTRA_DOWNLOAD, false);
        btnDownload.setVisibility(isDownload ? View.VISIBLE : View.GONE);
        if (status != null) {
            loadPhoto(status.getPath());
        }
    }

    private void initWindows() {
//        getWindow().getDecorView().setSystemUiVisibility( View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // menghilangkan blink saar aktif
            getWindow().setEnterTransition(null);
            // menghilangkan blink saar exit
            getWindow().setExitTransition(null);
        }
    }

    private void loadPhoto(String path) {
        Glide.with(this)
                .load(path)
                .into(imvPhoto);
    }

    private void downloadImage(Status status) {
        File file = new File(ConstantUtils.APP_DIRECTORY);
        boolean isDirectoryCreated = file.exists();

        if (!isDirectoryCreated) {
            isDirectoryCreated = file.mkdirs();
        }

        if (isDirectoryCreated) {
            File destFile = new File(String.format("%s%s%s", file, File.separator, status.getTitle()));
            boolean isDestCreated = file.exists();

            if (!isDestCreated) {
                isDestCreated = destFile.delete();
            }

            if (isDestCreated) {
                try {
                    copyFile(status.getFile(), destFile);
                } catch (IOException e) {
                    Snackbar.make(rootView, e.getMessage() != null ? e.getMessage() : "Error", Snackbar.LENGTH_SHORT).show();
                }

            }
        }
    }

    private void copyFile(File file, File destFile) throws IOException {
        if (destFile.getParentFile() != null) {
            boolean isDestCreated = destFile.getParentFile().exists();
            if (!isDestCreated) {
                isDestCreated = destFile.getParentFile().mkdirs();
            }

            if (isDestCreated) {
                isDestCreated = destFile.exists();

                if (!isDestCreated) {
                    isDestCreated = destFile.createNewFile();
                }

                if (isDestCreated) {
                    FileChannel source = new FileInputStream(file).getChannel();
                    FileChannel destination = new FileOutputStream(destFile).getChannel();
                    destination.transferFrom(source, 0, source.size());

                    source.close();
                    destination.close();
                    Snackbar.make(rootView, "Download complete", Snackbar.LENGTH_SHORT).show();
                }
            }
        }
    }

    private void share(Status status) {
        if (status != null) {
            Intent share = new Intent(Intent.ACTION_SEND);
            share.setType("image/jpeg");
            share.putExtra(Intent.EXTRA_STREAM, Uri.parse(status.getPath()));
            startActivity(Intent.createChooser(share, "Share Image"));
        }
    }

    @OnClick(R.id.btn_share)
    void onShareClicked() {
        if (status != null) {
            share(status);
        }
    }

    @OnClick(R.id.btn_download)
    void onDownloadClicked() {
        if (status != null) {
            downloadImage(status);
        }
    }
}