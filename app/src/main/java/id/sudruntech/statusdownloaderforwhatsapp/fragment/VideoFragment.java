package id.sudruntech.statusdownloaderforwhatsapp.fragment;

import android.content.Context;
import android.view.View;
import android.widget.ProgressBar;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import butterknife.BindView;
import id.sudruntech.base.BaseFragment;
import id.sudruntech.base.remote.DataView;
import id.sudruntech.statusdownloaderforwhatsapp.R;
import id.sudruntech.statusdownloaderforwhatsapp.adapter.VideoAdapter;
import id.sudruntech.statusdownloaderforwhatsapp.model.Status;
import id.sudruntech.statusdownloaderforwhatsapp.presenter.StatusPresenter;

public class VideoFragment extends BaseFragment {

    @BindView(R.id.root_view)
    ConstraintLayout rootView;
    @BindView(R.id.rv_videos)
    RecyclerView rvVideos;
    @BindView(R.id.pb_loading)
    ProgressBar pbLoading;

    private VideoAdapter videoAdapter;
    private StatusPresenter statusPresenter;

    public static VideoFragment newInstance() {
        return new VideoFragment();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_video;
    }

    @Override
    protected void initViews() {
        videoAdapter = new VideoAdapter(getBaseActivity());

        rvVideos.setHasFixedSize(true);
        rvVideos.setLayoutManager(new GridLayoutManager(getBaseActivity(), 3));
        rvVideos.setAdapter(videoAdapter);

        getVideosStatus();
    }

    private void showLoading(boolean isShow) {
        if (isShow) {
            getBaseActivity().runOnUiThread(() -> pbLoading.setVisibility(View.VISIBLE));
        } else {
            pbLoading.setVisibility(View.GONE);
        }
    }

    private void getVideosStatus() {
        statusPresenter = new StatusPresenter();
        statusPresenter.attachView(new DataView<List<Status>>() {
            @Override
            public Context getContext() {
                return getBaseActivity();
            }

            @Override
            public void isError(String message) {
                Snackbar.make(rootView, message, Snackbar.LENGTH_SHORT).show();
            }

            @Override
            public void showData(List<Status> data) {
                if (data.size() > 0) {
                    videoAdapter.addAll(data);
                } else {
                    // layout empty
                }
            }

            @Override
            public void showProgress() {
                showLoading(true);
            }

            @Override
            public void hideProgress() {
                showLoading(false);
            }
        });

        if (statusPresenter.isViewAttached()) {
            statusPresenter.getVideosStatus();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (statusPresenter != null) {
            statusPresenter.destroy();
            statusPresenter.detachView();
        }
    }
}