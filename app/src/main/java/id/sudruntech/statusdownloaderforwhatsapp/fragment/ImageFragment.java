package id.sudruntech.statusdownloaderforwhatsapp.fragment;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import butterknife.BindView;
import id.sudruntech.base.BaseFragment;
import id.sudruntech.base.remote.DataView;
import id.sudruntech.statusdownloaderforwhatsapp.ImageViewerActivity;
import id.sudruntech.statusdownloaderforwhatsapp.R;
import id.sudruntech.statusdownloaderforwhatsapp.adapter.ImageAdapter;
import id.sudruntech.statusdownloaderforwhatsapp.model.Status;
import id.sudruntech.statusdownloaderforwhatsapp.presenter.StatusPresenter;

public class ImageFragment extends BaseFragment {

    @BindView(R.id.root_view)
    ConstraintLayout rootView;
    @BindView(R.id.rv_images)
    RecyclerView rvImages;
    @BindView(R.id.pb_loading)
    ProgressBar pbLoading;

    private ImageAdapter imageAdapter;
    private StatusPresenter statusPresenter;

    public static ImageFragment newInstance() {
        return new ImageFragment();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_image;
    }

    @Override
    protected void initViews() {
        imageAdapter = new ImageAdapter(getBaseActivity());

        rvImages.setHasFixedSize(true);
        rvImages.setLayoutManager(new GridLayoutManager(getBaseActivity(), 3));
        rvImages.setAdapter(imageAdapter);

        initListener();
        getImagesStatus();
    }

    private void initListener() {
        imageAdapter.setOnItemClickListener((view, position) -> {
            RecyclerView.ViewHolder viewHolder = rvImages.findViewHolderForAdapterPosition(position);
            assert viewHolder != null;
            ImageView imvThumbnail = viewHolder.itemView.findViewById(R.id.imv_thumbnail);
            ImageViewerActivity.start(getBaseActivity(), imvThumbnail, true, imageAdapter.getData(position));
        });
    }

    private void showLoading(boolean isShow) {
        if (isShow) {
            getBaseActivity().runOnUiThread(() -> pbLoading.setVisibility(View.VISIBLE));
        } else {
            pbLoading.setVisibility(View.GONE);
        }
    }

    private void getImagesStatus() {
        statusPresenter = new StatusPresenter();
        statusPresenter.attachView(new DataView<List<Status>>() {
            @Override
            public Context getContext() {
                return getBaseActivity();
            }

            @Override
            public void isError(String message) {
                Snackbar.make(rootView, message, Snackbar.LENGTH_SHORT).show();
            }

            @Override
            public void showData(List<Status> data) {
                if (data.size() > 0) {
                    imageAdapter.addAll(data);
                } else {
                    // layout empty
                }
            }

            @Override
            public void showProgress() {
                showLoading(true);
            }

            @Override
            public void hideProgress() {
                showLoading(false);
            }
        });

        if (statusPresenter.isViewAttached()) {
            statusPresenter.getImagesStatus();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (statusPresenter != null) {
            statusPresenter.destroy();
            statusPresenter.detachView();
        }
    }
}