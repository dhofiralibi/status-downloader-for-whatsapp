package id.sudruntech.statusdownloaderforwhatsapp.presenter;

import android.os.Handler;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import id.sudruntech.base.remote.BasePresenter;
import id.sudruntech.base.remote.DataView;
import id.sudruntech.statusdownloaderforwhatsapp.model.Status;
import id.sudruntech.statusdownloaderforwhatsapp.utils.ConstantUtils;
import id.sudruntech.statusdownloaderforwhatsapp.utils.ThumbnailUtils;

public class StatusPresenter implements BasePresenter<DataView> {

    private DataView dataView;
    private Handler handler;

    public StatusPresenter() {
        handler = new Handler();
    }

    @Override
    public void attachView(DataView dataView) {
        this.dataView = dataView;
    }

    @Override
    public void detachView() {
        this.dataView = null;
    }

    @Override
    public boolean isViewAttached() {
        return this.dataView != null;
    }

    @Override
    public void destroy() {
        handler.removeCallbacksAndMessages(null);
    }

    public void getImagesStatus() {
        dataView.showProgress();
        if (ConstantUtils.STATUS_DIRECTORY.exists()) {
            new Thread(() -> {
                File[] files = ConstantUtils.STATUS_DIRECTORY.listFiles();
                List<Status> statuses = new ArrayList<>();
                if (files != null && files.length > 0) {
                    Arrays.sort(files);
                    for (File file : files) {
                        Status status = new Status(file, file.getName(), file.getAbsolutePath());
                        if (!status.isVideo()) {
                            status.setThumbnail(ThumbnailUtils.imageThumbnail(file));
                            statuses.add(status);
                        }
                    }
                    handler.post(() -> {
                        if (isViewAttached()) {
                            dataView.showData(statuses);
                            dataView.hideProgress();
                        }
                    });
                } else {
                    handler.post(() -> {
                        if (isViewAttached()) {
                            dataView.showData(statuses);
                            dataView.hideProgress();
                        }
                    });
                }
            }).start();
        } else {
            if (isViewAttached()) {
                dataView.hideProgress();
                dataView.isError("WhatsApp directory does not exist");
            }
        }
    }

    public void getVideosStatus() {
        dataView.showProgress();
        if (ConstantUtils.STATUS_DIRECTORY.exists()) {
            new Thread(() -> {
                File[] files = ConstantUtils.STATUS_DIRECTORY.listFiles();
                List<Status> statuses = new ArrayList<>();
                if (files != null && files.length > 0) {
                    Arrays.sort(files);
                    for (File file : files) {
                        Status status = new Status(file, file.getName(), file.getAbsolutePath());
                        if (status.isVideo()) {
                            status.setThumbnail(ThumbnailUtils.videoThumbnail(file));
                            statuses.add(status);
                        }
                    }
                    handler.post(() -> {
                        if (isViewAttached()) {
                            dataView.showData(statuses);
                            dataView.hideProgress();
                        }
                    });
                } else {
                    handler.post(() -> {
                        if (isViewAttached()) {
                            dataView.showData(statuses);
                            dataView.hideProgress();
                        }
                    });
                }
            }).start();
        } else {
            if (isViewAttached()) {
                dataView.hideProgress();
                dataView.isError("WhatsApp directory does not exist");
            }
        }
    }

    public void getDownloadStatus() {
        dataView.showProgress();
        File destFile = new File(ConstantUtils.APP_DIRECTORY);
        if (destFile.exists()) {
            new Thread(() -> {
                File[] files = destFile.listFiles();
                List<Status> statuses = new ArrayList<>();
                if (files != null && files.length > 0) {
                    Arrays.sort(files);
                    for (File file : files) {
                        Status status = new Status(file, file.getName(), file.getAbsolutePath());
                        if (!status.isVideo()) {
                            status.setThumbnail(ThumbnailUtils.imageThumbnail(file));
                            statuses.add(status);
                        } else {
                            status.setThumbnail(ThumbnailUtils.videoThumbnail(file));
                            statuses.add(status);
                        }
                    }
                    handler.post(() -> {
                        if (isViewAttached()) {
                            dataView.showData(statuses);
                            dataView.hideProgress();
                        }
                    });
                } else {
                    handler.post(() -> {
                        if (isViewAttached()) {
                            dataView.showData(statuses);
                            dataView.hideProgress();
                        }
                    });
                }
            }).start();
        } else {
            if (isViewAttached()) {
                dataView.hideProgress();
                dataView.isError("StatusDownloader directory does not exist");
            }
        }
    }
}
