package id.sudruntech.statusdownloaderforwhatsapp.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

import id.sudruntech.base.widget.CustomTextView;
import id.sudruntech.statusdownloaderforwhatsapp.R;

public class MainViewPager extends FragmentPagerAdapter {

    private List<Fragment> fragments;
    private List<String> titles;

    public MainViewPager(@NonNull FragmentManager fm, int behavior) {
        super(fm, behavior);
        fragments = new ArrayList<>();
        titles = new ArrayList<>();
    }

    public void addFragment(Fragment fragment, String title) {
        fragments.add(fragment);
        titles.add(title);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return titles.get(position);
    }

    public View getTabView(TabLayout tabLayout, int position) {
        ViewGroup viewGroup = (ViewGroup) tabLayout.getChildAt(0);
        View rootView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.tab_base, viewGroup, false);

        CustomTextView tvTitle = rootView.findViewById(id.sudruntech.base.R.id.tv_title);
        tvTitle.setText(titles.get(position));
        return rootView;
    }
}
