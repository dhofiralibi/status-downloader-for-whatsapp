package id.sudruntech.statusdownloaderforwhatsapp.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import id.sudruntech.base.adapter.BaseItemViewHolder;
import id.sudruntech.base.adapter.BaseRecyclerViewAdapter;
import id.sudruntech.base.listener.OnItemClickListener;
import id.sudruntech.statusdownloaderforwhatsapp.R;
import id.sudruntech.statusdownloaderforwhatsapp.model.Status;

public class VideoAdapter extends BaseRecyclerViewAdapter<Status, VideoAdapter.VideoItemViewHolder> {

    public VideoAdapter(Context context) {
        super(context);
    }

    @Override
    protected int getItemResourceLayout(int viewType) {
        return R.layout.list_item_status;
    }

    @NonNull
    @Override
    public VideoItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VideoItemViewHolder(getView(parent, viewType), onItemClickListener);
    }

    public class VideoItemViewHolder extends BaseItemViewHolder<Status> {

        @BindView(R.id.imv_thumbnail)
        ImageView imvThumbnail;

        VideoItemViewHolder(View itemView, OnItemClickListener onItemClickListener) {
            super(itemView, onItemClickListener);
        }

        @Override
        public void bind(Status item) {
            Glide.with(context)
                    .load(item.getThumbnail())
                    .centerCrop()
                    .into(imvThumbnail);
        }
    }
}
