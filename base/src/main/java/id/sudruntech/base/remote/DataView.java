package id.sudruntech.base.remote;

public interface DataView<T> extends BaseView {
    void showData(T data);
    void showProgress();
    void hideProgress();
}
