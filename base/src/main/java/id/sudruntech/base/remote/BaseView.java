package id.sudruntech.base.remote;

import android.content.Context;

public interface BaseView {
    Context getContext();
    void isError(String message);
}
