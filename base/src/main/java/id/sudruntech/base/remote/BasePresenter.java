package id.sudruntech.base.remote;

public interface BasePresenter<V extends BaseView> {
    void attachView(V view);
    void detachView();
    boolean isViewAttached();
    void destroy();
}
