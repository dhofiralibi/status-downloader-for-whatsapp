package id.sudruntech.base;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import butterknife.ButterKnife;

import static android.text.TextUtils.isEmpty;

public abstract class BaseFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(getLayoutId(), container, false);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    protected abstract
    @LayoutRes
    int getLayoutId();

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
    }

    protected void setActionBar(Toolbar toolbar, TextView textView, String title, boolean isShowTitle, boolean isShoHomeAsUp) {
        getBaseActivity().setSupportActionBar(toolbar);

        ActionBar actionBar = getBaseActivity().getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(isShoHomeAsUp);
            actionBar.setDisplayShowTitleEnabled(isShowTitle);
            if (!isEmpty(title)) {
                textView.setText(title);
            }
        }
    }

    protected BaseActivity getBaseActivity(){
        return (BaseActivity) getActivity();
    }

    protected abstract void initViews();
}
