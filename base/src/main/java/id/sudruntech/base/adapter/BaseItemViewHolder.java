package id.sudruntech.base.adapter;

import android.view.View;

import id.sudruntech.base.listener.OnItemClickListener;

public abstract class BaseItemViewHolder<T> extends BaseViewHolder implements View.OnClickListener {

    private OnItemClickListener onItemClickListener;

    public BaseItemViewHolder(View itemView, OnItemClickListener onItemClickListener) {
        super(itemView);
        this.onItemClickListener = onItemClickListener;
        itemView.setOnClickListener(this);
    }

    public abstract void bind(T item);

    @Override
    public void onClick(View view) {
        if(onItemClickListener != null) {
            onItemClickListener.onItemClicked(view, getAdapterPosition());
        }
    }
}
