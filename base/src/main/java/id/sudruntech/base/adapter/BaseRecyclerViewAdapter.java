package id.sudruntech.base.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import id.sudruntech.base.listener.OnItemClickListener;

public abstract class BaseRecyclerViewAdapter<T, VH extends BaseItemViewHolder> extends RecyclerView.Adapter<VH> {

    protected Context context;
    protected OnItemClickListener onItemClickListener;
    private int currentPosition = -1;
    protected List<T> datas;

    public BaseRecyclerViewAdapter(Context context) {
        this.context = context;
        datas = new ArrayList<>();
    }

    protected View getView(ViewGroup parent, int viewType) {
        return LayoutInflater.from(parent.getContext()).inflate(getItemResourceLayout(viewType), parent, false);
    }

    protected abstract int getItemResourceLayout(int viewType);

    @NonNull
    @Override
    public abstract VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType);

    @Override
    public void onBindViewHolder(VH holder, int position) {
        holder.bind(datas.get(position));
    }

    @Override
    public int getItemCount() {
        try {
            return datas.size();
        } catch (Exception e) {
            return 0;
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public T getData(int position) {
        return datas.get(position);
    }

    public List<T> getDatas() {
        return datas;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public int getCurrentPosition() {
        return currentPosition;
    }

    public void setCurrentPosition(int currentPosition) {
        this.currentPosition = currentPosition;
        notifyDataSetChanged();
    }

    public void add(T item) {
        datas.add(item);
        notifyItemInserted(datas.size() - 1);
    }

    public void addAll(List<T> items) {
        for (T item : items) {
            add(item);
        }
    }

    public void add(T item, int position) {
        datas.add(position, item);
        notifyItemInserted(position);
    }

    public void addOrUpdate(T item) {
        int i = datas.indexOf(item);
        if (i >= 0) {
            datas.set(i, item);
            notifyItemChanged(i);
        } else {
            add(item);
        }
    }

    public void addOrUpdate(T item, int position) {
        if (position >= 0) {
            datas.set(position, item);
            notifyItemChanged(position);
        }
    }

    public void addOrUpdate(List<T> items) {
        int size = items.size();
        for (int i = 0; i < size; i++) {
            T item = items.get(i);
            int x = datas.indexOf(item);
            if (x >= 0) {
                datas.set(x, item);
            } else {
                add(item);
            }
        }

        notifyDataSetChanged();
    }

    public void addOrUpdateFromTop(List<T> items) {
        int size = items.size();
        for (int i = 0; i < size; i++) {
            T item = items.get(i);
            int x = datas.indexOf(item);
            if (x >= 0) {
                datas.set(x, item);
            } else {
                add(item, 0);
            }
        }

        notifyDataSetChanged();
    }

    public void remove(int position) {
        if (position >= 0 && position < datas.size()) {
            datas.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void remove(T item) {
        int position = datas.indexOf(item);
        remove(position);
    }

    public void clear() {
        datas.clear();
        notifyDataSetChanged();
    }

    protected Context getContext() {
        return context;
    }
}
