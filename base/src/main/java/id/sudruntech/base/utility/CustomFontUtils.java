package id.sudruntech.base.utility;

import android.content.Context;
import android.graphics.Typeface;
import android.widget.TextView;

import id.sudruntech.base.R;

import static android.text.TextUtils.isEmpty;

public class CustomFontUtils {

    private static final int TYPE_NORMAL = 0;
    private static final int TYPE_BOLD = 1;
    private static final int TYPE_ITALIC = 2;

    public static void applyCustomFont(TextView textView, Context context) {
        if (textView.getTypeface() == null) {
            Typeface customFont = selectTypeface(context, Typeface.NORMAL);
            textView.setTypeface(customFont);
        } else {
            Typeface customFont = selectTypeface(context, textView.getTypeface().getStyle());
            textView.setTypeface(customFont);
        }
    }

    public static Typeface selectTypeface(Context context, int textStyle) {
        String fontName = "";
        switch (textStyle) {
            case TYPE_NORMAL:
                fontName = context.getResources().getString(R.string.font_normal);
                break;
            case TYPE_BOLD:
                fontName = context.getResources().getString(R.string.font_bold);
                break;
            case TYPE_ITALIC:
                fontName = context.getResources().getString(R.string.font_italic);
                break;
            default:
                fontName = context.getResources().getString(R.string.font_normal);
                break;
        }

        if (!isEmpty(fontName)) {
            return FontCache.getTypeface(fontName, context);
        } else {
            return null;
        }
    }
}
