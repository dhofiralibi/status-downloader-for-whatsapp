package id.sudruntech.base;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;

import java.lang.ref.WeakReference;

import butterknife.ButterKnife;
import id.sudruntech.base.listener.OnPermissionListener;

import static android.text.TextUtils.isEmpty;

public abstract class BaseActivity extends AppCompatActivity {

    public static final int REQUEST_STORAGE = 1;

    private WeakReference<OnPermissionListener> weakPermissionCallback;

    protected boolean getWindowFullScreen() {
        return false;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getWindowFullScreen()) {
            setWindowFullScreen();
        }
        setContentView(getLayoutId());
        injectViews();
    }

    protected abstract
    @LayoutRes
    int getLayoutId();

    protected void injectViews() {
        ButterKnife.bind(this);
        initViews();
    }

    private void setWindowFullScreen() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    protected void setActionBar(Toolbar toolbar, TextView textView, String title, boolean isShowTitle, boolean isShoHomeAsUp) {
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(isShoHomeAsUp);
            actionBar.setDisplayShowTitleEnabled(isShowTitle);
            if (!isEmpty(title)) {
                textView.setText(title);
            }
        }
    }

    protected abstract void initViews();

    public void storagePermission(OnPermissionListener onPermissionListener) {
        weakPermissionCallback = new WeakReference<>(onPermissionListener);

        String[] REQUEST_PERMISSION = {
                android.Manifest.permission.READ_EXTERNAL_STORAGE,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE
        };

        if(!checkPermission(REQUEST_PERMISSION)) {
            ActivityCompat.requestPermissions(this, REQUEST_PERMISSION, REQUEST_STORAGE);
        } else {
            weakPermissionCallback.get().onResultPermission(true);
        }
    }

    private boolean checkPermission(String... data) {
        if(data != null) {
            for (String permission : data) {
                if (ActivityCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }

        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_STORAGE) {
            if (weakPermissionCallback != null) {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    weakPermissionCallback.get().onResultPermission(true);
                } else {
                    weakPermissionCallback.get().onResultPermission(false);
                }
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
