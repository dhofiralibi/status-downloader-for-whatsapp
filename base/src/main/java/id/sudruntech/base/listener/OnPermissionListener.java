package id.sudruntech.base.listener;

public interface OnPermissionListener {
    void onResultPermission(boolean granted);
}
