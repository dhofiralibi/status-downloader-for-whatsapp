package id.sudruntech.base.listener;

import android.view.View;

public interface OnItemClickListener {
    void onItemClicked(View view, int position);
}
