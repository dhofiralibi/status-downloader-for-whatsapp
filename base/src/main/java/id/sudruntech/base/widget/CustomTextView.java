package id.sudruntech.base.widget;

import android.content.Context;
import android.util.AttributeSet;

import id.sudruntech.base.utility.CustomFontUtils;

public class CustomTextView extends androidx.appcompat.widget.AppCompatTextView {

    public CustomTextView(Context context) {
        super(context);

        CustomFontUtils.applyCustomFont(this, context);
    }

    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        CustomFontUtils.applyCustomFont(this, context);
    }

    public CustomTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        CustomFontUtils.applyCustomFont(this, context);
    }
}
